// Bài tập 1
document.getElementById("btnTimSoNguyenDuong").onclick = function () {
  // Tạo một biến để chứa tổng của các số nguyên dương
  var sum = 0;
  // // Output là số nguyên duong nhỏ nhất, cũng chỉnh là yêu tố bước nhảy
  var soHang = 0;
  // Thiết lập điều kiện lặp: Nếu sum < 1000 thì vòng lặp được tiếp tục
  while (sum < 10000) {
    // Khối xử lý: sum sẽ cộng với biến bước nhảy sau mỗi lần lặp
    sum += soHang;
    // Thay đổi giá trị của biến bước nhảy trong mỗi lần lặp
    soHang++;
  }

  document.getElementById("soNguyenDuong").innerHTML =
    "Số nguyên dương nhỏ nhất: " + soHang;
};

// Bài tập 2
document.getElementById("btnTinhTong").onclick = function () {
  // Input: number, 2 số  x và y
  var soX = Number(document.getElementById("nhapSoX").value);
  var soN = Number(document.getElementById("nhapSoN").value);
  // Output: tổng của 2 số
  tongS = 0;

  // Xác định yếu tố bước nhảy
  i = 1;
  // Điều kiện lặp: nếu i <= soN thì sẽ ngừng lặp
  while (i <= soN) {
    // Xử lý
    tongS += Math.pow(soX, i);
    //  Thay đổi điều kiện lặp
    i++;
  }

  document.getElementById("soTong").innerHTML = "Tổng: " + tongS;
};

// Bài tập 3
document.getElementById("btnTinhGiaiThua").onclick = function () {
  //input: number
  var nhapSo = Number(document.getElementById("nhapSoN_2").value);

  //output: giaiThua = 1;
  var giaiThua = 1;
  //Xác định yếu tố thay đổi khởi tạo nó
  var giaTri = 1;
  //Thiết lập điều kiện lặp
  while (giaTri <= nhapSo) {
    //Điều kiện lặp
    // giaiThua = giaiThua *giaTri
    // Xử lý
    giaiThua *= giaTri;
    // Thay đổi điều kiện lặp
    giaTri++;
  }

  document.getElementById("soGiaiThua").innerHTML = "Giai thừa: " + giaiThua;
};

// Bài tập 4:
document.getElementById("btnTaoThe").onclick = function () {
  var content = "";
  var divLe = '<div style="background-color: blue; color: white;">Div lẻ</div>';
  var divChan =
    '<div style="background-color: red; color: white;">Div chẵn</div>';
  var soHang = 1;
  while (soHang <= 10) {
    if (soHang % 2 === 0) {
      content += divChan;
    } else {
      content += divLe;
    }
    soHang++;
  }
  document.getElementById("theDiv").innerHTML = content;
};

// Bài tập 5:
document.getElementById("btnInSoNguyenTo").onclick = function () {
  // Input: number
  var n = Number(document.getElementById("nhapSoN_3").value);

  // Output: string
  var ketQua = '';

  for (var i = 2; i <= n; i++) {
    var checkSNT = kiemTraSoNguyenTo(i);
    if (checkSNT){
        ketQua += i + ' ';
    }
  }
  document.getElementById('soNguyenTo').innerHTML = "Kết quả: " + ketQua; 
};

function kiemTraSoNguyenTo(number){
    var checkSNT = true;

    for(var i = 2; i <= Math.sqrt(number); i++){
        if (number % i === 0){
            checkSNT = false;
            break;
        }
    }

    return checkSNT;
}
